package com.example.netmonlitedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.netmonlitedemo.model.Report;
import com.example.netmonlitedemo.model.Session;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ContentResolver mContentResolver;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = findViewById(R.id.text_field);
        mContentResolver = getContentResolver();

        //Should not be done in UI thread. This is just for example simplification.
        ArrayList<Session> sessionArrayList = loadSessions();
        String sessionsInfo = "Number of stored sessions: " + sessionArrayList.size();
        Log.d(TAG, sessionsInfo);

        String sessionAndReportsInfo = "N/A";
        if(sessionArrayList.size()>0) {
            String sessionName = sessionArrayList.get(0).getSessionName();
            long startReportId = sessionArrayList.get(0).getStartRepId();
            long stopReportId = sessionArrayList.get(0).getStopRepId();
            //Should not be done in UI thread. This is just for example simplification.
            ArrayList<Report> reportArrayList = loadReports(startReportId, stopReportId);
            sessionAndReportsInfo = "Session: " + sessionName +"\nStart Report Id: " + startReportId
                    +"\nStop Report Id: " + stopReportId + "\nReports: " + reportArrayList.size();
            Log.d(TAG, sessionAndReportsInfo);
        }

        mTextView.setText(sessionsInfo + "\n\n" + sessionAndReportsInfo);
    }

    public synchronized ArrayList<Session> loadSessions() {
        ArrayList<Session> sessions = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = mContentResolver.query(NetMonLiteProviderHelper.SESSION_CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    sessions.add(NetMonLiteProviderHelper.getSessionFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception encountered while loadSessions: " + e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return sessions;
    }

    public synchronized ArrayList<Report> loadReports(long startId, long stopId) {
        ArrayList<Report> reports = new ArrayList<>();
        String selection = NetMonLiteProviderHelper.REPORT_ID + " BETWEEN " + startId + " AND " + stopId;
        String sortOrder = NetMonLiteProviderHelper.REPORT_ID + " ASC";
        Cursor cursor = null;
        try {
            cursor = mContentResolver.query(NetMonLiteProviderHelper.REPORT_CONTENT_URI, null, selection, null, sortOrder);
            if (cursor!= null && cursor.moveToFirst()) {
                do {
                    reports.add(NetMonLiteProviderHelper.getReportFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception encountered while loadReports: " + e);
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return reports;
    }

}