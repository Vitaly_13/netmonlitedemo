package com.example.netmonlitedemo.model;

/**
 * Created by VVA on 04.08.2020.
 */
public class Report {

    private long mId;
    private long mSessionId;
    private int mReport;
    private long mSysTime;
    private int mSimState;
    private String mNetOpName;
    private String mNetOpCode;
    private int mRoaming;
    private int mNetType;
    private int mCallState;
    private int mDataState;
    private int mDataAct;
    private long mDataRx;
    private long mDataTx;
    private long mNstartId;
    private long mNstopId;
    private int mNgsm;
    private int mNumts;
    private int mNlte;
    private int mNrssi;
    private int mNregCells;
    private int mTech;
    private int mMcc;
    private int mMnc;
    private int mLacTac;
    private int mNodeId;
    private int mCid;
    private int mPscPci;
    private int mRssi;
    private int mRsrq;
    private int mRssnr;
    private int mSlev;
    private int mGps;
    private int mAccur;
    private double mLat;
    private double mLong;
    private double mClat;
    private double mClong;
    private String mBand;
    private int mArfcn;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getSessionId() {
        return mSessionId;
    }

    public void setSessionId(long sessionId) {
        mSessionId = sessionId;
    }

    public int getReport() {
        return mReport;
    }

    public void setReport(int report) {
        mReport = report;
    }

    public long getSysTime() {
        return mSysTime;
    }

    public void setSysTime(long sysTime) {
        mSysTime = sysTime;
    }

    public int getSimState() {
        return mSimState;
    }

    public void setSimState(int simState) {
        mSimState = simState;
    }

    public String getNetOpName() {
        return mNetOpName;
    }

    public void setNetOpName(String netOpName) {
        mNetOpName = netOpName;
    }

    public String getNetOpCode() {
        return mNetOpCode;
    }

    public void setNetOpCode(String netOpCode) {
        mNetOpCode = netOpCode;
    }

    public int getRoaming() {
        return mRoaming;
    }

    public void setRoaming(int roaming) {
        mRoaming = roaming;
    }

    public int getNetType() {
        return mNetType;
    }

    public void setNetType(int netType) {
        mNetType = netType;
    }

    public int getCallState() {
        return mCallState;
    }

    public void setCallState(int callState) {
        mCallState = callState;
    }

    public int getDataState() {
        return mDataState;
    }

    public void setDataState(int dataState) {
        mDataState = dataState;
    }

    public int getDataAct() {
        return mDataAct;
    }

    public void setDataAct(int dataAct) {
        mDataAct = dataAct;
    }

    public long getDataRx() {
        return mDataRx;
    }

    public void setDataRx(long dataRx) {
        mDataRx = dataRx;
    }

    public long getDataTx() {
        return mDataTx;
    }

    public void setDataTx(long dataTx) {
        mDataTx = dataTx;
    }

    public long getNstartId() {
        return mNstartId;
    }

    public void setNstartId(long nstartId) {
        mNstartId = nstartId;
    }

    public long getNstopId() {
        return mNstopId;
    }

    public void setNstopId(long nstopId) {
        mNstopId = nstopId;
    }

    public int getNgsm() {
        return mNgsm;
    }

    public void setNgsm(int ngsm) {
        mNgsm = ngsm;
    }

    public int getNumts() {
        return mNumts;
    }

    public void setNumts(int numts) {
        mNumts = numts;
    }

    public int getNlte() {
        return mNlte;
    }

    public void setNlte(int nlte) {
        mNlte = nlte;
    }

    public int getNrssi() {
        return mNrssi;
    }

    public void setNrssi(int nrssi) {
        mNrssi = nrssi;
    }

    public int getNregCells() {
        return mNregCells;
    }

    public void setNregCells(int nregCells) {
        mNregCells = nregCells;
    }

    public int getTech() {
        return mTech;
    }

    public void setTech(int tech) {
        mTech = tech;
    }

    public int getMcc() {
        return mMcc;
    }

    public void setMcc(int mcc) {
        mMcc = mcc;
    }

    public int getMnc() {
        return mMnc;
    }

    public void setMnc(int mnc) {
        mMnc = mnc;
    }

    public int getLacTac() {
        return mLacTac;
    }

    public void setLacTac(int lacTac) {
        mLacTac = lacTac;
    }

    public int getNodeId() {
        return mNodeId;
    }

    public void setNodeId(int nodeId) {
        mNodeId = nodeId;
    }

    public int getCid() {
        return mCid;
    }

    public void setCid(int cid) {
        mCid = cid;
    }

    public int getPscPci() {
        return mPscPci;
    }

    public void setPscPci(int pscPci) {
        mPscPci = pscPci;
    }

    public int getRssi() {
        return mRssi;
    }

    public void setRssi(int rssi) {
        mRssi = rssi;
    }

    public int getRsrq() {
        return mRsrq;
    }

    public void setRsrq(int rsrq) {
        mRsrq = rsrq;
    }

    public int getRssnr() {
        return mRssnr;
    }

    public void setRssnr(int rssnr) {
        mRssnr = rssnr;
    }

    public int getSlev() {
        return mSlev;
    }

    public void setSlev(int slev) {
        mSlev = slev;
    }

    public int getGps() {
        return mGps;
    }

    public void setGps(int gps) {
        mGps = gps;
    }

    public int getAccur() {
        return mAccur;
    }

    public void setAccur(int accur) {
        mAccur = accur;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double lat) {
        mLat = lat;
    }

    public double getLong() {
        return mLong;
    }

    public void setLong(double aLong) {
        mLong = aLong;
    }

    public double getClat() {
        return mClat;
    }

    public void setClat(double clat) {
        mClat = clat;
    }

    public double getClong() {
        return mClong;
    }

    public void setClong(double clong) {
        mClong = clong;
    }

    public String getBand() {
        return mBand;
    }

    public void setBand(String band) {
        mBand = band;
    }

    public int getArfcn() {
        return mArfcn;
    }

    public void setArfcn(int arfcn) {
        mArfcn = arfcn;
    }

}
