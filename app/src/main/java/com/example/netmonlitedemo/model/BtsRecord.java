package com.example.netmonlitedemo.model;

/**
 * Created by VVA on 04.08.2020.
 */
public class BtsRecord {
    private long mId;
    private int mTech;
    private int mMcc;
    private int mMnc;
    private int mLacTac;
    private int mNodeId;
    private int mCid;
    private int mPscPci;
    private String mBand;
    private int mArfcn;
    private String mSiteName;
    private double mSiteLat;
    private double mSiteLong;
    private String mCellName;
    private double mAzimuth;
    private double mHeight;
    private double mTiltMech;
    private double mTiltEl;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public int getTech() {
        return mTech;
    }

    public void setTech(int tech) {
        mTech = tech;
    }

    public int getMcc() {
        return mMcc;
    }

    public void setMcc(int mcc) {
        mMcc = mcc;
    }

    public int getMnc() {
        return mMnc;
    }

    public void setMnc(int mnc) {
        mMnc = mnc;
    }

    public int getLacTac() {
        return mLacTac;
    }

    public void setLacTac(int lacTac) {
        mLacTac = lacTac;
    }

    public int getNodeId() {
        return mNodeId;
    }

    public void setNodeId(int nodeId) {
        mNodeId = nodeId;
    }

    public int getCid() {
        return mCid;
    }

    public void setCid(int cid) {
        mCid = cid;
    }

    public int getPscPci() {
        return mPscPci;
    }

    public void setPscPci(int pscPci) {
        mPscPci = pscPci;
    }

    public String getBand() {
        return mBand;
    }

    public void setBand(String band) {
        mBand = band;
    }

    public int getArfcn() {
        return mArfcn;
    }

    public void setArfcn(int arfcn) {
        mArfcn = arfcn;
    }

    public String getSiteName() {
        return mSiteName;
    }

    public void setSiteName(String siteName) {
        mSiteName = siteName;
    }

    public double getSiteLat() {
        return mSiteLat;
    }

    public void setSiteLat(double siteLat) {
        mSiteLat = siteLat;
    }

    public double getSiteLong() {
        return mSiteLong;
    }

    public void setSiteLong(double siteLong) {
        mSiteLong = siteLong;
    }

    public String getCellName() {
        return mCellName;
    }

    public void setCellName(String cellName) {
        mCellName = cellName;
    }

    public double getAzimuth() {
        return mAzimuth;
    }

    public void setAzimuth(double azimuth) {
        mAzimuth = azimuth;
    }

    public double getHeight() {
        return mHeight;
    }

    public void setHeight(double height) {
        mHeight = height;
    }

    public double getTiltMech() {
        return mTiltMech;
    }

    public void setTiltMech(double tiltMech) {
        mTiltMech = tiltMech;
    }

    public double getTiltEl() {
        return mTiltEl;
    }

    public void setTiltEl(double tiltEl) {
        mTiltEl = tiltEl;
    }
}
