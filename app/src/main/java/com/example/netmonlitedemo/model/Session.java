package com.example.netmonlitedemo.model;

/**
 * Created by VVA on 04.08.2020.
 */
public class Session {
    private long mId;
    private String mSessionName;
    private long mStartTime;
    private long mStopTime;
    private int mActive;
    private String mImei;
    private String mDevSw;
    private String mSimOpName;
    private String mSimOpCode;
    private String mSimSerial;
    private String mImsi;
    private String mAndroidVer;
    private String mDevName;
    private long mStartRepId;
    private long mStopRepId;
    private int mLteRepNum;
    private int mUmtsRepNum;
    private int mGsmRepNum;
    private int mUnknRepNum;
    private double mLatMin;
    private double mLatMax;
    private double mLongMin;
    private double mLongMax;
    private int mGsmRssi110;
    private int mGsmRssi105;
    private int mGsmRssi100;
    private int mGsmRssi95;
    private int mGsmRssi90;
    private int mGsmRssi85;
    private int mGsmRssi80;
    private int mGsmRssi75;
    private int mGsmRssi70;
    private int mGsmRssi0;
    private int mUmtsRssi110;
    private int mUmtsRssi105;
    private int mUmtsRssi100;
    private int mUmtsRssi95;
    private int mUmtsRssi90;
    private int mUmtsRssi85;
    private int mUmtsRssi80;
    private int mUmtsRssi75;
    private int mUmtsRssi70;
    private int mUmtsRssi0;
    private int mLteRsrp120;
    private int mLteRsrp115;
    private int mLteRsrp110;
    private int mLteRsrp105;
    private int mLteRsrp100;
    private int mLteRsrp95;
    private int mLteRsrp90;
    private int mLteRsrp85;
    private int mLteRsrp80;
    private int mLteRsrp0;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getSessionName() {
        return mSessionName;
    }

    public void setSessionName(String sessionName) {
        mSessionName = sessionName;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    public long getStopTime() {
        return mStopTime;
    }

    public void setStopTime(long stopTime) {
        mStopTime = stopTime;
    }

    public int getActive() {
        return mActive;
    }

    public void setActive(int active) {
        mActive = active;
    }

    public String getImei() {
        return mImei;
    }

    public void setImei(String imei) {
        mImei = imei;
    }

    public String getDevSw() {
        return mDevSw;
    }

    public void setDevSw(String devSw) {
        mDevSw = devSw;
    }

    public String getSimOpName() {
        return mSimOpName;
    }

    public void setSimOpName(String simOpName) {
        mSimOpName = simOpName;
    }

    public String getSimOpCode() {
        return mSimOpCode;
    }

    public void setSimOpCode(String simOpCode) {
        mSimOpCode = simOpCode;
    }

    public String getSimSerial() {
        return mSimSerial;
    }

    public void setSimSerial(String simSerial) {
        mSimSerial = simSerial;
    }

    public String getImsi() {
        return mImsi;
    }

    public void setImsi(String imsi) {
        mImsi = imsi;
    }

    public String getAndroidVer() {
        return mAndroidVer;
    }

    public void setAndroidVer(String androidVer) {
        mAndroidVer = androidVer;
    }

    public String getDevName() {
        return mDevName;
    }

    public void setDevName(String devName) {
        mDevName = devName;
    }

    public long getStartRepId() {
        return mStartRepId;
    }

    public void setStartRepId(long startRepId) {
        mStartRepId = startRepId;
    }

    public long getStopRepId() {
        return mStopRepId;
    }

    public void setStopRepId(long stopRepId) {
        mStopRepId = stopRepId;
    }

    public int getLteRepNum() {
        return mLteRepNum;
    }

    public void setLteRepNum(int lteRepNum) {
        mLteRepNum = lteRepNum;
    }

    public int getUmtsRepNum() {
        return mUmtsRepNum;
    }

    public void setUmtsRepNum(int umtsRepNum) {
        mUmtsRepNum = umtsRepNum;
    }

    public int getGsmRepNum() {
        return mGsmRepNum;
    }

    public void setGsmRepNum(int gsmRepNum) {
        mGsmRepNum = gsmRepNum;
    }

    public int getUnknRepNum() {
        return mUnknRepNum;
    }

    public void setUnknRepNum(int unknRepNum) {
        mUnknRepNum = unknRepNum;
    }

    public double getLatMin() {
        return mLatMin;
    }

    public void setLatMin(double latMin) {
        mLatMin = latMin;
    }

    public double getLatMax() {
        return mLatMax;
    }

    public void setLatMax(double latMax) {
        mLatMax = latMax;
    }

    public double getLongMin() {
        return mLongMin;
    }

    public void setLongMin(double longMin) {
        mLongMin = longMin;
    }

    public double getLongMax() {
        return mLongMax;
    }

    public void setLongMax(double longMax) {
        mLongMax = longMax;
    }

    public int getGsmRssi110() {
        return mGsmRssi110;
    }

    public void setGsmRssi110(int gsmRssi110) {
        mGsmRssi110 = gsmRssi110;
    }

    public int getGsmRssi105() {
        return mGsmRssi105;
    }

    public void setGsmRssi105(int gsmRssi105) {
        mGsmRssi105 = gsmRssi105;
    }

    public int getGsmRssi100() {
        return mGsmRssi100;
    }

    public void setGsmRssi100(int gsmRssi100) {
        mGsmRssi100 = gsmRssi100;
    }

    public int getGsmRssi95() {
        return mGsmRssi95;
    }

    public void setGsmRssi95(int gsmRssi95) {
        mGsmRssi95 = gsmRssi95;
    }

    public int getGsmRssi90() {
        return mGsmRssi90;
    }

    public void setGsmRssi90(int gsmRssi90) {
        mGsmRssi90 = gsmRssi90;
    }

    public int getGsmRssi85() {
        return mGsmRssi85;
    }

    public void setGsmRssi85(int gsmRssi85) {
        mGsmRssi85 = gsmRssi85;
    }

    public int getGsmRssi80() {
        return mGsmRssi80;
    }

    public void setGsmRssi80(int gsmRssi80) {
        mGsmRssi80 = gsmRssi80;
    }

    public int getGsmRssi75() {
        return mGsmRssi75;
    }

    public void setGsmRssi75(int gsmRssi75) {
        mGsmRssi75 = gsmRssi75;
    }

    public int getGsmRssi70() {
        return mGsmRssi70;
    }

    public void setGsmRssi70(int gsmRssi70) {
        mGsmRssi70 = gsmRssi70;
    }

    public int getGsmRssi0() {
        return mGsmRssi0;
    }

    public void setGsmRssi0(int gsmRssi0) {
        mGsmRssi0 = gsmRssi0;
    }

    public int getUmtsRssi110() {
        return mUmtsRssi110;
    }

    public void setUmtsRssi110(int umtsRssi110) {
        mUmtsRssi110 = umtsRssi110;
    }

    public int getUmtsRssi105() {
        return mUmtsRssi105;
    }

    public void setUmtsRssi105(int umtsRssi105) {
        mUmtsRssi105 = umtsRssi105;
    }

    public int getUmtsRssi100() {
        return mUmtsRssi100;
    }

    public void setUmtsRssi100(int umtsRssi100) {
        mUmtsRssi100 = umtsRssi100;
    }

    public int getUmtsRssi95() {
        return mUmtsRssi95;
    }

    public void setUmtsRssi95(int umtsRssi95) {
        mUmtsRssi95 = umtsRssi95;
    }

    public int getUmtsRssi90() {
        return mUmtsRssi90;
    }

    public void setUmtsRssi90(int umtsRssi90) {
        mUmtsRssi90 = umtsRssi90;
    }

    public int getUmtsRssi85() {
        return mUmtsRssi85;
    }

    public void setUmtsRssi85(int umtsRssi85) {
        mUmtsRssi85 = umtsRssi85;
    }

    public int getUmtsRssi80() {
        return mUmtsRssi80;
    }

    public void setUmtsRssi80(int umtsRssi80) {
        mUmtsRssi80 = umtsRssi80;
    }

    public int getUmtsRssi75() {
        return mUmtsRssi75;
    }

    public void setUmtsRssi75(int umtsRssi75) {
        mUmtsRssi75 = umtsRssi75;
    }

    public int getUmtsRssi70() {
        return mUmtsRssi70;
    }

    public void setUmtsRssi70(int umtsRssi70) {
        mUmtsRssi70 = umtsRssi70;
    }

    public int getUmtsRssi0() {
        return mUmtsRssi0;
    }

    public void setUmtsRssi0(int umtsRssi0) {
        mUmtsRssi0 = umtsRssi0;
    }

    public int getLteRsrp120() {
        return mLteRsrp120;
    }

    public void setLteRsrp120(int lteRsrp120) {
        mLteRsrp120 = lteRsrp120;
    }

    public int getLteRsrp115() {
        return mLteRsrp115;
    }

    public void setLteRsrp115(int lteRsrp115) {
        mLteRsrp115 = lteRsrp115;
    }

    public int getLteRsrp110() {
        return mLteRsrp110;
    }

    public void setLteRsrp110(int lteRsrp110) {
        mLteRsrp110 = lteRsrp110;
    }

    public int getLteRsrp105() {
        return mLteRsrp105;
    }

    public void setLteRsrp105(int lteRsrp105) {
        mLteRsrp105 = lteRsrp105;
    }

    public int getLteRsrp100() {
        return mLteRsrp100;
    }

    public void setLteRsrp100(int lteRsrp100) {
        mLteRsrp100 = lteRsrp100;
    }

    public int getLteRsrp95() {
        return mLteRsrp95;
    }

    public void setLteRsrp95(int lteRsrp95) {
        mLteRsrp95 = lteRsrp95;
    }

    public int getLteRsrp90() {
        return mLteRsrp90;
    }

    public void setLteRsrp90(int lteRsrp90) {
        mLteRsrp90 = lteRsrp90;
    }

    public int getLteRsrp85() {
        return mLteRsrp85;
    }

    public void setLteRsrp85(int lteRsrp85) {
        mLteRsrp85 = lteRsrp85;
    }

    public int getLteRsrp80() {
        return mLteRsrp80;
    }

    public void setLteRsrp80(int lteRsrp80) {
        mLteRsrp80 = lteRsrp80;
    }

    public int getLteRsrp0() {
        return mLteRsrp0;
    }

    public void setLteRsrp0(int lteRsrp0) {
        mLteRsrp0 = lteRsrp0;
    }

}
