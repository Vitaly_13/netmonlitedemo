package com.example.netmonlitedemo;

import android.database.Cursor;
import android.net.Uri;

import com.example.netmonlitedemo.model.BtsRecord;
import com.example.netmonlitedemo.model.Report;
import com.example.netmonlitedemo.model.Session;

public class NetMonLiteProviderHelper {

    // // Uri
    // authority
    public static final String AUTHORITY = "ru.v_a_v.netmonitor.provider";

    // path
    public static final String SESSION_PATH = "sessions";
    public static final String REPORT_PATH = "reports";
    public static final String BTS_PATH = "bts";

    // Uri
    public static final Uri SESSION_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + SESSION_PATH);
    public static final Uri REPORT_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + REPORT_PATH);
    public static final Uri BTS_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BTS_PATH);

    // Data types
    // All
    public static final String SESSION_CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + SESSION_PATH;
    public static final String REPORT_CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + REPORT_PATH;
    public static final String BTS_CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + BTS_PATH;

    // One row
    public static final String SESSION_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + AUTHORITY + "." + SESSION_PATH;
    public static final String REPORT_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + AUTHORITY + "." + REPORT_PATH;
    public static final String BTS_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + AUTHORITY + "." + BTS_PATH;

    public static final String SESSION_ID =         "_id";
    public static final String SESSION_SESSIONNAME ="sessionname";
    public static final String SESSION_STARTTIME =  "starttime";
    public static final String SESSION_STOPTIME =   "stoptime";
    public static final String SESSION_ACTIVE =     "active";
    public static final String SESSION_IMEI =       "imei";
    public static final String SESSION_DEVSW =      "devsw";
    public static final String SESSION_SIMOPNAME =  "simopname";
    public static final String SESSION_SIMOPCODE =  "simopcode";
    public static final String SESSION_SIMSERIAL =  "simserial";
    public static final String SESSION_IMSI =       "imsi";
    public static final String SESSION_ANDROIDVER = "androidver";
    public static final String SESSION_DEVNAME =    "devname";
    public static final String SESSION_STARTREPID = "startrepid";
    public static final String SESSION_STOPREPID =  "stoprepid";
    public static final String SESSION_LTEREPNUM =  "lterepnum";
    public static final String SESSION_UMTSREPNUM = "umtsrepnum";
    public static final String SESSION_GSMREPNUM =  "gsmrepnum";
    public static final String SESSION_UNKNREPNUM = "unknrepnum";
    public static final String SESSION_LATMIN =     "latmin";
    public static final String SESSION_LATMAX =     "latmax";
    public static final String SESSION_LONGMIN =    "longmin";
    public static final String SESSION_LONGMAX =    "longmax";
    public static final String SESSION_GSMRSSI110 = "gsmrssi110";
    public static final String SESSION_GSMRSSI105 = "gsmrssi105";
    public static final String SESSION_GSMRSSI100 = "gsmrssi100";
    public static final String SESSION_GSMRSSI95 =  "gsmrssi95";
    public static final String SESSION_GSMRSSI90 =  "gsmrssi90";
    public static final String SESSION_GSMRSSI85 =  "gsmrssi85";
    public static final String SESSION_GSMRSSI80 =  "gsmrssi80";
    public static final String SESSION_GSMRSSI75 =  "gsmrssi75";
    public static final String SESSION_GSMRSSI70 =  "gsmrssi70";
    public static final String SESSION_GSMRSSI0 =   "gsmrssi0";
    public static final String SESSION_UMTSRSSI110 ="umtsrssi110";
    public static final String SESSION_UMTSRSSI105 ="umtsrssi105";
    public static final String SESSION_UMTSRSSI100 ="umtsrssi100";
    public static final String SESSION_UMTSRSSI95 = "umtsrssi95";
    public static final String SESSION_UMTSRSSI90 = "umtsrssi90";
    public static final String SESSION_UMTSRSSI85 = "umtsrssi85";
    public static final String SESSION_UMTSRSSI80 = "umtsrssi80";
    public static final String SESSION_UMTSRSSI75 = "umtsrssi75";
    public static final String SESSION_UMTSRSSI70 = "umtsrssi70";
    public static final String SESSION_UMTSRSSI0 =  "umtsrssi0";
    public static final String SESSION_LTERSRP120 = "ltersrp120";
    public static final String SESSION_LTERSRP115 = "ltersrp115";
    public static final String SESSION_LTERSRP110 = "ltersrp110";
    public static final String SESSION_LTERSRP105 = "ltersrp105";
    public static final String SESSION_LTERSRP100 = "ltersrp100";
    public static final String SESSION_LTERSRP95 =  "ltersrp95";
    public static final String SESSION_LTERSRP90 =  "ltersrp90";
    public static final String SESSION_LTERSRP85 =  "ltersrp85";
    public static final String SESSION_LTERSRP80 =  "ltersrp80";
    public static final String SESSION_LTERSRP0 =   "ltersrp0";

    public static final String REPORT_ID =         "_id";
    public static final String REPORT_SESSIONID =  "sessionid";
    public static final String REPORT_REPORT =     "report";
    public static final String REPORT_SYSTIME =    "systime";
    public static final String REPORT_SIMSTATE =   "simstate";
    public static final String REPORT_NETOPNAME =  "netopname";
    public static final String REPORT_NETOPCODE =  "netopcode";
    public static final String REPORT_ROAMING =    "roaming";
    public static final String REPORT_NETTYPE =    "nettype";
    public static final String REPORT_CALLSTATE =  "callstate";
    public static final String REPORT_DATASTATE =  "datastate";
    public static final String REPORT_DATAACT =    "dataact";
    public static final String REPORT_DATARX   =   "datarx";
    public static final String REPORT_DATATX   =   "datatx";
    public static final String REPORT_NSTARTID =   "nstartid";
    public static final String REPORT_NSTOPID  =   "nstopid";
    public static final String REPORT_NGSM     =   "ngsm";
    public static final String REPORT_NUMTS    =   "numts";
    public static final String REPORT_NLTE     =   "nlte";
    public static final String REPORT_NRSSI    =   "nrssi";
    public static final String REPORT_NREGCELLS =  "nregcells";
    public static final String REPORT_TECH =       "tech";
    public static final String REPORT_MCC =        "mcc";
    public static final String REPORT_MNC =        "mnc";
    public static final String REPORT_LACTAC =     "lactac";
    public static final String REPORT_NODEID =     "nodeid";
    public static final String REPORT_CID =        "cid";
    public static final String REPORT_PSCPCI =     "pscpci";
    public static final String REPORT_RSSI =       "rssi";
    public static final String REPORT_RSRQ =       "rsrq";
    public static final String REPORT_RSSNR =      "rssnr";
    public static final String REPORT_SLEV =       "slev";
    public static final String REPORT_GPS =        "gps";
    public static final String REPORT_ACCUR =      "accur";
    public static final String REPORT_LAT =        "lat";
    public static final String REPORT_LONG =       "long";
    public static final String REPORT_CLAT =       "clat";
    public static final String REPORT_CLONG =      "clong";
    public static final String REPORT_BAND             ="band";
    public static final String REPORT_ARFCN            ="arfcn";

    public static final String BTS_ID               ="_id";
    public static final String BTS_TECH             ="tech";
    public static final String BTS_MCC              ="mcc";
    public static final String BTS_MNC              ="mnc";
    public static final String BTS_LACTAC           ="lactac";
    public static final String BTS_NODEID           ="nodeid";
    public static final String BTS_CID              ="cid";
    public static final String BTS_PSCPCI           ="pscpci";
    public static final String BTS_BAND             ="band";
    public static final String BTS_ARFCN            ="arfcn";
    public static final String BTS_SITE_NAME        ="sitename";
    public static final String BTS_SITE_LAT         ="sitelat";
    public static final String BTS_SITE_LONG        ="sitelong";
    public static final String BTS_CELL_NAME        ="cellname";
    public static final String BTS_AZIMUTH          ="azimuth";
    public static final String BTS_HEIGHT           ="height";
    public static final String BTS_TILT_M           ="tiltmech";
    public static final String BTS_TILT_E           ="tiltel";

    public static synchronized Session getSessionFromCursor(Cursor cursor) {
        Session session = new Session();
        session.setId(cursor.getLong(cursor.getColumnIndex(SESSION_ID)));
        session.setSessionName(cursor.getString(cursor.getColumnIndex(SESSION_SESSIONNAME)));
        session.setStartTime(cursor.getLong(cursor.getColumnIndex(SESSION_STARTTIME)));
        session.setStopTime(cursor.getLong(cursor.getColumnIndex(SESSION_STOPTIME)));
        session.setActive(cursor.getInt(cursor.getColumnIndex(SESSION_ACTIVE)));
        session.setImei(cursor.getString(cursor.getColumnIndex(SESSION_IMEI)));
        session.setDevSw(cursor.getString(cursor.getColumnIndex(SESSION_DEVSW)));
        session.setSimOpName(cursor.getString(cursor.getColumnIndex(SESSION_SIMOPNAME)));
        session.setSimOpCode(cursor.getString(cursor.getColumnIndex(SESSION_SIMOPCODE)));
        session.setSimSerial(cursor.getString(cursor.getColumnIndex(SESSION_SIMSERIAL)));
        session.setImsi(cursor.getString(cursor.getColumnIndex(SESSION_IMSI)));
        session.setAndroidVer(cursor.getString(cursor.getColumnIndex(SESSION_ANDROIDVER)));
        session.setDevName(cursor.getString(cursor.getColumnIndex(SESSION_DEVNAME)));
        session.setStartRepId(cursor.getLong(cursor.getColumnIndex(SESSION_STARTREPID)));
        session.setStopRepId(cursor.getLong(cursor.getColumnIndex(SESSION_STOPREPID)));
        session.setLteRepNum(cursor.getInt(cursor.getColumnIndex(SESSION_LTEREPNUM)));
        session.setUmtsRepNum(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSREPNUM)));
        session.setGsmRepNum(cursor.getInt(cursor.getColumnIndex(SESSION_GSMREPNUM)));
        session.setUnknRepNum(cursor.getInt(cursor.getColumnIndex(SESSION_UNKNREPNUM)));
        session.setLatMin(cursor.getDouble(cursor.getColumnIndex(SESSION_LATMIN)));
        session.setLatMax(cursor.getDouble(cursor.getColumnIndex(SESSION_LATMAX)));
        session.setLongMin(cursor.getDouble(cursor.getColumnIndex(SESSION_LONGMIN)));
        session.setLongMax(cursor.getDouble(cursor.getColumnIndex(SESSION_LONGMAX)));
        session.setGsmRssi110(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI110)));
        session.setGsmRssi105(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI105)));
        session.setGsmRssi100(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI100)));
        session.setGsmRssi95(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI95)));
        session.setGsmRssi90(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI90)));
        session.setGsmRssi85(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI85)));
        session.setGsmRssi80(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI80)));
        session.setGsmRssi75(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI75)));
        session.setGsmRssi70(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI70)));
        session.setGsmRssi0(cursor.getInt(cursor.getColumnIndex(SESSION_GSMRSSI0)));
        session.setUmtsRssi110(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI110)));
        session.setUmtsRssi105(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI105)));
        session.setUmtsRssi100(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI100)));
        session.setUmtsRssi95(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI95)));
        session.setUmtsRssi90(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI90)));
        session.setUmtsRssi85(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI85)));
        session.setUmtsRssi80(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI80)));
        session.setUmtsRssi75(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI75)));
        session.setUmtsRssi70(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI70)));
        session.setUmtsRssi0(cursor.getInt(cursor.getColumnIndex(SESSION_UMTSRSSI0)));
        session.setLteRsrp120(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP120)));
        session.setLteRsrp115(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP115)));
        session.setLteRsrp110(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP110)));
        session.setLteRsrp105(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP105)));
        session.setLteRsrp100(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP100)));
        session.setLteRsrp95(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP95)));
        session.setLteRsrp90(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP90)));
        session.setLteRsrp85(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP85)));
        session.setLteRsrp80(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP80)));
        session.setLteRsrp0(cursor.getInt(cursor.getColumnIndex(SESSION_LTERSRP0)));
        return session;
    }

    public static synchronized Report getReportFromCursor(Cursor cursor) {
        Report report = new Report();
        report.setId(cursor.getLong(cursor.getColumnIndex(REPORT_ID)));
        report.setSessionId(cursor.getLong(cursor.getColumnIndex(REPORT_SESSIONID)));
        report.setReport(cursor.getInt(cursor.getColumnIndex(REPORT_REPORT)));
        report.setSysTime(cursor.getLong(cursor.getColumnIndex(REPORT_SYSTIME)));
        report.setSimState(cursor.getInt(cursor.getColumnIndex(REPORT_SIMSTATE)));
        report.setNetOpName(cursor.getString(cursor.getColumnIndex(REPORT_NETOPNAME)));
        report.setNetOpCode(cursor.getString(cursor.getColumnIndex(REPORT_NETOPCODE)));
        report.setRoaming(cursor.getInt(cursor.getColumnIndex(REPORT_ROAMING)));
        report.setNetType(cursor.getInt(cursor.getColumnIndex(REPORT_NETTYPE)));
        report.setCallState(cursor.getInt(cursor.getColumnIndex(REPORT_CALLSTATE)));
        report.setDataState(cursor.getInt(cursor.getColumnIndex(REPORT_DATASTATE)));
        report.setDataAct(cursor.getInt(cursor.getColumnIndex(REPORT_DATAACT)));
        report.setDataRx(cursor.getLong(cursor.getColumnIndex(REPORT_DATARX)));
        report.setDataTx(cursor.getLong(cursor.getColumnIndex(REPORT_DATATX)));
        report.setNstartId(cursor.getLong(cursor.getColumnIndex(REPORT_NSTARTID)));
        report.setNstopId(cursor.getLong(cursor.getColumnIndex(REPORT_NSTOPID)));
        report.setNgsm(cursor.getInt(cursor.getColumnIndex(REPORT_NGSM)));
        report.setNumts(cursor.getInt(cursor.getColumnIndex(REPORT_NUMTS)));
        report.setNlte(cursor.getInt(cursor.getColumnIndex(REPORT_NLTE)));
        report.setNrssi(cursor.getInt(cursor.getColumnIndex(REPORT_NRSSI)));
        report.setNregCells(cursor.getInt(cursor.getColumnIndex(REPORT_NREGCELLS)));
        report.setTech(cursor.getInt(cursor.getColumnIndex(REPORT_TECH)));
        report.setMcc(cursor.getInt(cursor.getColumnIndex(REPORT_MCC)));
        report.setMnc(cursor.getInt(cursor.getColumnIndex(REPORT_MNC)));
        report.setLacTac(cursor.getInt(cursor.getColumnIndex(REPORT_LACTAC)));
        report.setNodeId(cursor.getInt(cursor.getColumnIndex(REPORT_NODEID)));
        report.setCid(cursor.getInt(cursor.getColumnIndex(REPORT_CID)));
        report.setPscPci(cursor.getInt(cursor.getColumnIndex(REPORT_PSCPCI)));
        report.setRssi(cursor.getInt(cursor.getColumnIndex(REPORT_RSSI)));
        report.setRsrq(cursor.getInt(cursor.getColumnIndex(REPORT_RSRQ)));
        report.setRssnr(cursor.getInt(cursor.getColumnIndex(REPORT_RSSNR)));
        report.setSlev(cursor.getInt(cursor.getColumnIndex(REPORT_SLEV)));
        report.setGps(cursor.getInt(cursor.getColumnIndex(REPORT_GPS)));
        report.setAccur(cursor.getInt(cursor.getColumnIndex(REPORT_ACCUR)));
        report.setLat(cursor.getDouble(cursor.getColumnIndex(REPORT_LAT)));
        report.setLong(cursor.getDouble(cursor.getColumnIndex(REPORT_LONG)));
        report.setClat(cursor.getDouble(cursor.getColumnIndex(REPORT_CLAT)));
        report.setClong(cursor.getDouble(cursor.getColumnIndex(REPORT_CLONG)));
        report.setBand(cursor.getString(cursor.getColumnIndex(REPORT_BAND)));
        report.setArfcn(cursor.getInt(cursor.getColumnIndex(REPORT_ARFCN)));
        return report;
    }

    public static synchronized BtsRecord getBtsRecordFromCursor(Cursor cursor) {
        BtsRecord btsRecord = new BtsRecord();
        btsRecord.setId(cursor.getLong(cursor.getColumnIndex(BTS_ID)));
        btsRecord.setTech(cursor.getInt(cursor.getColumnIndex(BTS_TECH)));
        btsRecord.setMcc(cursor.getInt(cursor.getColumnIndex(BTS_MCC)));
        btsRecord.setMnc(cursor.getInt(cursor.getColumnIndex(BTS_MNC)));
        btsRecord.setLacTac(cursor.getInt(cursor.getColumnIndex(BTS_LACTAC)));
        String tmpString = cursor.getString(cursor.getColumnIndex(BTS_NODEID));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setNodeId(cursor.getInt(cursor.getColumnIndex(BTS_NODEID)));
        } else {
            btsRecord.setNodeId(Integer.MAX_VALUE);
        }
        btsRecord.setCid(cursor.getInt(cursor.getColumnIndex(BTS_CID)));
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_PSCPCI));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setPscPci(cursor.getInt(cursor.getColumnIndex(BTS_PSCPCI)));
        } else {
            btsRecord.setPscPci(Integer.MAX_VALUE);
        }
        btsRecord.setBand(cursor.getString(cursor.getColumnIndex(BTS_BAND)));
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_ARFCN));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setArfcn(cursor.getInt(cursor.getColumnIndex(BTS_ARFCN)));
        } else {
            btsRecord.setArfcn(Integer.MAX_VALUE);
        }
        btsRecord.setSiteName(cursor.getString(cursor.getColumnIndex(BTS_SITE_NAME)));
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_SITE_LAT));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setSiteLat(cursor.getDouble(cursor.getColumnIndex(BTS_SITE_LAT)));
        } else {
            btsRecord.setSiteLat(Double.MAX_VALUE);
        }
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_SITE_LONG));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setSiteLong(cursor.getDouble(cursor.getColumnIndex(BTS_SITE_LONG)));
        } else {
            btsRecord.setSiteLong(Double.MAX_VALUE);
        }
        btsRecord.setCellName(cursor.getString(cursor.getColumnIndex(BTS_CELL_NAME)));
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_AZIMUTH));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setAzimuth(cursor.getDouble(cursor.getColumnIndex(BTS_AZIMUTH)));
        } else {
            btsRecord.setAzimuth(Double.MAX_VALUE);
        }
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_HEIGHT));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setHeight(cursor.getDouble(cursor.getColumnIndex(BTS_HEIGHT)));
        } else {
            btsRecord.setHeight(Double.MAX_VALUE);
        }
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_TILT_M));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setTiltMech(cursor.getDouble(cursor.getColumnIndex(BTS_TILT_M)));
        } else {
            btsRecord.setTiltMech(Double.MAX_VALUE);
        }
        tmpString = cursor.getString(cursor.getColumnIndex(BTS_TILT_E));
        if (tmpString != null && tmpString.length() > 0) {
            btsRecord.setTiltEl(cursor.getDouble(cursor.getColumnIndex(BTS_TILT_E)));
        } else {
            btsRecord.setTiltEl(Double.MAX_VALUE);
        }
        return btsRecord;
    }

}
